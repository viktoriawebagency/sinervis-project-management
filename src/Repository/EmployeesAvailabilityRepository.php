<?php

namespace App\Repository;

use App\Entity\EmployeesAvailability;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method EmployeesAvailability|null find($id, $lockMode = null, $lockVersion = null)
 * @method EmployeesAvailability|null findOneBy(array $criteria, array $orderBy = null)
 * @method EmployeesAvailability[]    findAll()
 * @method EmployeesAvailability[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmployeesAvailabilityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EmployeesAvailability::class);
    }

    // /**
    //  * @return EmployeesAvailability[] Returns an array of EmployeesAvailability objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EmployeesAvailability
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
