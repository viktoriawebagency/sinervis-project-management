<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190925163953 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE contact (id INT AUTO_INCREMENT NOT NULL, country_id INT DEFAULT NULL, region_id INT DEFAULT NULL, city_id INT DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, surname VARCHAR(255) DEFAULT NULL, company_name VARCHAR(255) DEFAULT NULL, address VARCHAR(255) DEFAULT NULL, fiscal_code VARCHAR(16) DEFAULT NULL, vat_number VARCHAR(20) DEFAULT NULL, phone_number VARCHAR(50) DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, type VARCHAR(3) NOT NULL, hourly_cost INT DEFAULT NULL, INDEX IDX_4C62E638F92F3E70 (country_id), INDEX IDX_4C62E63898260155 (region_id), INDEX IDX_4C62E6388BAC62AF (city_id), INDEX type_idx (type), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE geo__name (id INT NOT NULL, country_id INT DEFAULT NULL, admin1_id INT DEFAULT NULL, admin2_id INT DEFAULT NULL, admin3_id INT DEFAULT NULL, admin4_id INT DEFAULT NULL, timezone_id INT DEFAULT NULL, name VARCHAR(200) NOT NULL, ascii_name VARCHAR(200) DEFAULT NULL, latitude DOUBLE PRECISION DEFAULT NULL, longitude DOUBLE PRECISION DEFAULT NULL, feature_class VARCHAR(1) DEFAULT NULL, feature_code VARCHAR(10) DEFAULT NULL, country_code VARCHAR(2) DEFAULT NULL, cc2 VARCHAR(200) DEFAULT NULL, population BIGINT DEFAULT NULL, elevation INT DEFAULT NULL, dem INT DEFAULT NULL, modification_date DATE DEFAULT NULL, INDEX IDX_3022932F92F3E70 (country_id), INDEX IDX_3022932CBBF1675 (admin1_id), INDEX IDX_3022932D90AB99B (admin2_id), INDEX IDX_302293261B6DEFE (admin3_id), INDEX IDX_3022932FC61E647 (admin4_id), INDEX IDX_30229323FE997DE (timezone_id), INDEX geoname_geoname_search_idx (name, country_code), INDEX geoname_feature_code_idx (feature_code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE geo__country (id INT NOT NULL, geoname_id INT DEFAULT NULL, iso VARCHAR(2) NOT NULL, iso3 VARCHAR(3) NOT NULL, iso_numeric INT NOT NULL, fips VARCHAR(2) DEFAULT NULL, name VARCHAR(255) NOT NULL, capital VARCHAR(255) DEFAULT NULL, area BIGINT NOT NULL, population BIGINT NOT NULL, tld VARCHAR(15) DEFAULT NULL, currency VARCHAR(3) DEFAULT NULL, currency_name VARCHAR(50) DEFAULT NULL, phone_prefix INT DEFAULT NULL, postal_format LONGTEXT DEFAULT NULL, postal_regex LONGTEXT DEFAULT NULL, languages JSON DEFAULT NULL COMMENT \'(DC2Type:json_array)\', INDEX IDX_C9825FBA23F5422B (geoname_id), INDEX geoname_country_search_idx (name, iso), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE geo__administrative (id INT AUTO_INCREMENT NOT NULL, geoname_id INT DEFAULT NULL, code VARCHAR(30) NOT NULL, name VARCHAR(200) NOT NULL, ascii_name VARCHAR(200) DEFAULT NULL, UNIQUE INDEX UNIQ_93FA99CA77153098 (code), INDEX IDX_93FA99CA23F5422B (geoname_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE geo__name_hierarchy (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, child_id INT DEFAULT NULL, INDEX IDX_867FF2AE727ACA70 (parent_id), INDEX IDX_867FF2AEDD62C21B (child_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE geo__timezone (id INT AUTO_INCREMENT NOT NULL, timezone VARCHAR(50) NOT NULL, country_code VARCHAR(2) NOT NULL, gmt_offset DOUBLE PRECISION NOT NULL, dst_offset DOUBLE PRECISION NOT NULL, raw_offset DOUBLE PRECISION NOT NULL, UNIQUE INDEX UNIQ_B8FEDDFE3701B297 (timezone), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE contact ADD CONSTRAINT FK_4C62E638F92F3E70 FOREIGN KEY (country_id) REFERENCES geo__country (id)');
        $this->addSql('ALTER TABLE contact ADD CONSTRAINT FK_4C62E63898260155 FOREIGN KEY (region_id) REFERENCES geo__name (id)');
        $this->addSql('ALTER TABLE contact ADD CONSTRAINT FK_4C62E6388BAC62AF FOREIGN KEY (city_id) REFERENCES geo__name (id)');
        $this->addSql('ALTER TABLE geo__name ADD CONSTRAINT FK_3022932F92F3E70 FOREIGN KEY (country_id) REFERENCES geo__country (id)');
        $this->addSql('ALTER TABLE geo__name ADD CONSTRAINT FK_3022932CBBF1675 FOREIGN KEY (admin1_id) REFERENCES geo__administrative (id)');
        $this->addSql('ALTER TABLE geo__name ADD CONSTRAINT FK_3022932D90AB99B FOREIGN KEY (admin2_id) REFERENCES geo__administrative (id)');
        $this->addSql('ALTER TABLE geo__name ADD CONSTRAINT FK_302293261B6DEFE FOREIGN KEY (admin3_id) REFERENCES geo__administrative (id)');
        $this->addSql('ALTER TABLE geo__name ADD CONSTRAINT FK_3022932FC61E647 FOREIGN KEY (admin4_id) REFERENCES geo__administrative (id)');
        $this->addSql('ALTER TABLE geo__name ADD CONSTRAINT FK_30229323FE997DE FOREIGN KEY (timezone_id) REFERENCES geo__timezone (id)');
        $this->addSql('ALTER TABLE geo__country ADD CONSTRAINT FK_C9825FBA23F5422B FOREIGN KEY (geoname_id) REFERENCES geo__name (id)');
        $this->addSql('ALTER TABLE geo__administrative ADD CONSTRAINT FK_93FA99CA23F5422B FOREIGN KEY (geoname_id) REFERENCES geo__name (id)');
        $this->addSql('ALTER TABLE geo__name_hierarchy ADD CONSTRAINT FK_867FF2AE727ACA70 FOREIGN KEY (parent_id) REFERENCES geo__name (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE geo__name_hierarchy ADD CONSTRAINT FK_867FF2AEDD62C21B FOREIGN KEY (child_id) REFERENCES geo__name (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contact DROP FOREIGN KEY FK_4C62E63898260155');
        $this->addSql('ALTER TABLE contact DROP FOREIGN KEY FK_4C62E6388BAC62AF');
        $this->addSql('ALTER TABLE geo__country DROP FOREIGN KEY FK_C9825FBA23F5422B');
        $this->addSql('ALTER TABLE geo__administrative DROP FOREIGN KEY FK_93FA99CA23F5422B');
        $this->addSql('ALTER TABLE geo__name_hierarchy DROP FOREIGN KEY FK_867FF2AE727ACA70');
        $this->addSql('ALTER TABLE geo__name_hierarchy DROP FOREIGN KEY FK_867FF2AEDD62C21B');
        $this->addSql('ALTER TABLE contact DROP FOREIGN KEY FK_4C62E638F92F3E70');
        $this->addSql('ALTER TABLE geo__name DROP FOREIGN KEY FK_3022932F92F3E70');
        $this->addSql('ALTER TABLE geo__name DROP FOREIGN KEY FK_3022932CBBF1675');
        $this->addSql('ALTER TABLE geo__name DROP FOREIGN KEY FK_3022932D90AB99B');
        $this->addSql('ALTER TABLE geo__name DROP FOREIGN KEY FK_302293261B6DEFE');
        $this->addSql('ALTER TABLE geo__name DROP FOREIGN KEY FK_3022932FC61E647');
        $this->addSql('ALTER TABLE geo__name DROP FOREIGN KEY FK_30229323FE997DE');
        $this->addSql('DROP TABLE contact');
        $this->addSql('DROP TABLE geo__name');
        $this->addSql('DROP TABLE geo__country');
        $this->addSql('DROP TABLE geo__administrative');
        $this->addSql('DROP TABLE geo__name_hierarchy');
        $this->addSql('DROP TABLE geo__timezone');
    }
}
