<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190929130217 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE employee_favorite_regions_geoname (employee_id INT NOT NULL, geo_name_id INT NOT NULL, INDEX IDX_5855124D8C03F15C (employee_id), INDEX IDX_5855124DC60111D4 (geo_name_id), PRIMARY KEY(employee_id, geo_name_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE employee_favorite_cities_geoname (employee_id INT NOT NULL, geo_name_id INT NOT NULL, INDEX IDX_41BB809C8C03F15C (employee_id), INDEX IDX_41BB809CC60111D4 (geo_name_id), PRIMARY KEY(employee_id, geo_name_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE employee_favorite_regions_geoname ADD CONSTRAINT FK_5855124D8C03F15C FOREIGN KEY (employee_id) REFERENCES contact (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE employee_favorite_regions_geoname ADD CONSTRAINT FK_5855124DC60111D4 FOREIGN KEY (geo_name_id) REFERENCES geo__name (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE employee_favorite_cities_geoname ADD CONSTRAINT FK_41BB809C8C03F15C FOREIGN KEY (employee_id) REFERENCES contact (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE employee_favorite_cities_geoname ADD CONSTRAINT FK_41BB809CC60111D4 FOREIGN KEY (geo_name_id) REFERENCES geo__name (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE employee_favorite_regions_geoname');
        $this->addSql('DROP TABLE employee_favorite_cities_geoname');
    }
}
