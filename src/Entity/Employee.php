<?php

namespace App\Entity;

use Bordeux\Bundle\GeoNameBundle\Entity\GeoName;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EmployeeRepository")
 */
class Employee extends Contact
{
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $hourly_cost;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\ContractType", mappedBy="employee")
     */
    private $contractTypes;

    /**
     * @ORM\ManyToMany(targetEntity="Bordeux\Bundle\GeoNameBundle\Entity\GeoName")
     * @ORM\JoinTable(name="employee_favorite_regions_geoname")
     */
    private $favorite_regions;

    /**
     * @ORM\ManyToMany(targetEntity="Bordeux\Bundle\GeoNameBundle\Entity\GeoName")
     * @ORM\JoinTable(name="employee_favorite_cities_geoname")
     */
    private $favorite_cities;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\EmployeesAvailability", mappedBy="employee", orphanRemoval=true)
     */
    private $employeesAvailabilities;

    public function __construct()
    {
        $this->contractTypes = new ArrayCollection();
        $this->favorite_regions = new ArrayCollection();
        $this->favorite_cities = new ArrayCollection();
        $this->employeesAvailabilities = new ArrayCollection();
    }

    public function getHourlyCost(): ?int
    {
        return $this->hourly_cost;
    }

    public function setHourlyCost(?int $hourly_cost): self
    {
        $this->hourly_cost = $hourly_cost;

        return $this;
    }

    /**
     * @return Collection|ContractType[]
     */
    public function getContractTypes(): Collection
    {
        return $this->contractTypes;
    }

    public function addContractType(ContractType $contractType): self
    {
        if (!$this->contractTypes->contains($contractType)) {
            $this->contractTypes[] = $contractType;
            $contractType->addEmployee($this);
        }

        return $this;
    }

    public function removeContractType(ContractType $contractType): self
    {
        if ($this->contractTypes->contains($contractType)) {
            $this->contractTypes->removeElement($contractType);
            $contractType->removeEmployee($this);
        }

        return $this;
    }

    /**
     * @return Collection|GeoName[]
     */
    public function getFavoriteRegions(): Collection
    {
        return $this->favorite_regions;
    }

    public function addFavoriteRegion(GeoName $favoriteRegion): self
    {
        if (!$this->favorite_regions->contains($favoriteRegion)) {
            $this->favorite_regions[] = $favoriteRegion;
        }

        return $this;
    }

    public function removeFavoriteRegion(GeoName $favoriteRegion): self
    {
        if ($this->favorite_regions->contains($favoriteRegion)) {
            $this->favorite_regions->removeElement($favoriteRegion);
        }

        return $this;
    }

    /**
     * @return Collection|GeoName[]
     */
    public function getFavoriteCities(): Collection
    {
        return $this->favorite_cities;
    }

    public function addFavoriteCity(GeoName $favoriteCity): self
    {
        if (!$this->favorite_cities->contains($favoriteCity)) {
            $this->favorite_cities[] = $favoriteCity;
        }

        return $this;
    }

    public function removeFavoriteCity(GeoName $favoriteCity): self
    {
        if ($this->favorite_cities->contains($favoriteCity)) {
            $this->favorite_cities->removeElement($favoriteCity);
        }

        return $this;
    }

    /**
     * @return Collection|EmployeesAvailability[]
     */
    public function getEmployeesAvailabilities(): Collection
    {
        return $this->employeesAvailabilities;
    }

    public function addEmployeesAvailability(EmployeesAvailability $employeesAvailability): self
    {
        if (!$this->employeesAvailabilities->contains($employeesAvailability)) {
            $this->employeesAvailabilities[] = $employeesAvailability;
            $employeesAvailability->setEmployee($this);
        }

        return $this;
    }

    public function removeEmployeesAvailability(EmployeesAvailability $employeesAvailability): self
    {
        if ($this->employeesAvailabilities->contains($employeesAvailability)) {
            $this->employeesAvailabilities->removeElement($employeesAvailability);
            // set the owning side to null (unless already changed)
            if ($employeesAvailability->getEmployee() === $this) {
                $employeesAvailability->setEmployee(null);
            }
        }

        return $this;
    }    
}
